import tkinter as tk;
from tkinter import ttk;
from PIL import Image;
import pageGame as pgGame;

#Iconos e imagenes tomadas del siguiente sitio
# <a href="https://www.flaticon.com/free-icons/ghost" title="ghost icons">Ghost icons created by Freepik - Flaticon</a>
# <a href="https://www.flaticon.com/free-icons/fear" title="fear icons">Fear icons created by Smashicons - Flaticon</a>
# <a href="https://www.flaticon.com/free-icons/question" title="question icons">Question icons created by Freepik - Flaticon</a>
# <a href="https://www.flaticon.com/free-icons/x" title="x icons">X icons created by Stockio - Flaticon</a>
# <a href="https://www.flaticon.com/free-icons/ghost" title="ghost icons">Ghost icons created by Freepik - Flaticon</a>
# <a href="https://www.freepik.com" title="Freepik"> Freepik </a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com'</a>
# <a href="https://www.flaticon.com/free-icons/ghost" title="ghost icons">Ghost icons created by Freepik - Flaticon</a>

class pageIni(ttk.Frame):
    #Creación de ventana principal
    def __init__(self, mainWindow):
        super().__init__(mainWindow);
        #Variables globales
        self.bgMain = "#0D0D0D";
        self.bgBtn = "#758283";
        #Asignación de título
        mainWindow.title("Juego de fantasmas");
        #Creación de imagenes
        self.imgGhost = tk.PhotoImage(file="Assets/ghostIni.png");
        #Tuplas con configuración de fuentes a usar
        self.styleFontTitle = ("Malgun Gothic", 36, "bold");
        self.styleFontTitleRules = ("Malgun Gothic", 28, "bold");
        self.styleFontDescRules = ("Malgun Gothic", 20);
        self.styleFontCopy = ("Malgun Gothic", 16);
        self.styleFontBtnIni = ("Malgun Gothic", 26, "bold");
        #Creación de frames y asignación a ventana principal
        self.frameMain = tk.Frame(mainWindow, background=self.bgMain, width=1200, height=900);
        self.frameMain.pack();
        #Creación de textos
        self.txtTitle = tk.Label(self.frameMain, text="Juego de fantasmas", font=self.styleFontTitle, bg=self.bgMain, fg="#FFF");
        self.txtTitle.place(x=360, y=40);
        #Asignación de imagen a label y a la ventana principal
        self.lblImgGhoost = tk.Label(self.frameMain, image=self.imgGhost, bg=self.bgMain);
        self.lblImgGhoost.image = self.imgGhost;
        self.lblImgGhoost.place(x=60, y=140);
        self.txtTitleRules = tk.Label(self.frameMain, text="Reglas:", font=self.styleFontTitleRules, bg=self.bgMain, fg="#FFF");
        self.txtTitleRules.place(x=620, y=220);
        self.txtTitleRules = tk.Label(self.frameMain, text="- Inicias con 10 puntos.\n- Si al seleccionar una casilla\nencuentras un fantasma pierdes\n4 puntos, si no encuentras un\nfantasma ganas 2 puntos.\n- Si acumulas 3 fantasmas\ninmediatamente pierdes.\n- Para ganar tienes que alcanzar\nuna puntuación mínima de 50 puntos.", justify="left", font=self.styleFontDescRules, bg=self.bgMain, fg="#FFF");
        self.txtTitleRules.place(x=620, y=340);
        self.txtDesc = tk.Label(self.frameMain, text="Evita los fantasmas y ganas", font=self.styleFontTitleRules, bg=self.bgMain, fg="#FFF");
        self.txtDesc.place(x=350, y=780);
        self.txtCopy = tk.Label(self.frameMain, text="Creado por Antonio Patiño", font=self.styleFontCopy, bg=self.bgMain, fg="#FFF");
        self.txtCopy.place(x=30, y=840);
        #Creación de botones
        self.btnIni = tk.Button(self.frameMain, text="Iniciar", font=self.styleFontBtnIni, bg=self.bgBtn, bd=0, highlightthickness=0, cursor="hand1", command=lambda: self.openWindowGame(mainWindow));
        self.btnIni.place(x=260, y=680);
        #Regla para evitar que se redimensione ventana principal
        mainWindow.resizable(height=False, width=False);
    #Método que abre ventana para iniciar el Juego
    def openWindowGame(self, mainWindow):
        mainWindow.destroy();
        windowGame = tk.Tk()
        appGame = pgGame.pageGame(windowGame);
        appGame.mainloop();

#Creación de ventana
mainWindow = tk.Tk();
app = pageIni(mainWindow);
app.mainloop();
