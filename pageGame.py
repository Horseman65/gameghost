import tkinter as tk;
from tkinter import ttk;
from tkinter import messagebox as MessageBox;
from PIL import Image;
import random;

#Iconos e imagenes tomadas del siguiente sitio
# <a href="https://www.flaticon.com/free-icons/ghost" title="ghost icons">Ghost icons created by Freepik - Flaticon</a>
# <a href="https://www.flaticon.com/free-icons/fear" title="fear icons">Fear icons created by Smashicons - Flaticon</a>
# <a href="https://www.flaticon.com/free-icons/question" title="question icons">Question icons created by Freepik - Flaticon</a>
# <a href="https://www.flaticon.com/free-icons/x" title="x icons">X icons created by Stockio - Flaticon</a>
# <a href="https://www.flaticon.com/free-icons/ghost" title="ghost icons">Ghost icons created by Freepik - Flaticon</a>
# <a href="https://www.freepik.com" title="Freepik"> Freepik </a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com'</a>
# <a href="https://www.flaticon.com/free-icons/ghost" title="ghost icons">Ghost icons created by Freepik - Flaticon</a>

class pageGame(ttk.Frame):
    #Creación de ventana principal
    def __init__(self, mainWindow):
        super().__init__(mainWindow);
        #Variables globales
        self.bgMain = "#242B2E";
        self.bgBtn_1 = "#CAD5E2";
        self.bgBtn_2 = "#FFFFFF";
        #Contador de fantasmas encontrados
        self.ghostsFinds = 0;
        #Arreglo que guarda la matriz de fantasmas
        self.listData = [];
        #Tuplas con configuración de fuentes a usar
        self.styleFontTitle = ("Malgun Gothic", 24, "bold");
        self.styleFontDescScore = ("Malgun Gothic", 20, "bold");
        self.styleFontScore = ("Malgun Gothic", 36, "bold");
        #Asignación de título
        mainWindow.title("Inicio de juego");
        #Creación de frames y asignación a ventana principal
        self.frameMain = tk.Frame(mainWindow, background=self.bgMain, width=900, height=980);
        self.frameMain.pack();
        self.framePaint = tk.Frame(self.frameMain, background=self.bgBtn_1, width=420, height=700);
        self.framePaint.place(x=220, y=110);
        #Creación de imagenes
        self.imgGhost_1 = tk.PhotoImage(file="Assets/ghost_1.png");
        self.imgGhost_2 = tk.PhotoImage(file="Assets/ghost_2.png");
        self.imgReset = tk.PhotoImage(file="Assets/reset.png");
        self.imgQuestion = tk.PhotoImage(file="Assets/question.png");
        #Creación de textos
        self.txtTitle = tk.Label(self.frameMain, text="Elige la casilla que desees", font=self.styleFontTitle, bg=self.bgMain, fg="#FFF");
        self.txtTitle.place(x=190, y=30);
        self.txtDescScore = tk.Label(self.frameMain, text="Puntuación:", font=self.styleFontDescScore, bg=self.bgMain, fg="#FFF");
        self.txtDescScore.place(x=680, y=430);
        self.txtScore = tk.Label(self.frameMain, text="10", font=self.styleFontScore, bg=self.bgMain, fg="#FFF");
        self.txtScore.place(x=740, y=480);
        #Asignación de imagen a label y a la ventana principal
        self.lblImgGhost_1 = tk.Label(self.frameMain, image=self.imgGhost_1, bg=self.bgMain);
        self.lblImgGhost_1.image = self.imgGhost_1;
        self.lblImgGhost_1.place(x=60, y=40);
        self.lblImgGhost_2 = tk.Label(self.frameMain, image=self.imgGhost_1, bg=self.bgMain);
        self.lblImgGhost_2.image = self.imgGhost_1;
        self.lblImgGhost_2.place(x=740, y=40);
        self.lblImgGhost_3 = tk.Label(self.frameMain, image=self.imgGhost_1, bg=self.bgMain);
        self.lblImgGhost_3.image = self.imgGhost_1;
        self.lblImgGhost_3.place(x=60, y=850);
        self.lblImgGhost_4 = tk.Label(self.frameMain, image=self.imgGhost_1, bg=self.bgMain);
        self.lblImgGhost_4.image = self.imgGhost_1;
        self.lblImgGhost_4.place(x=740, y=850);
        self.lblImgGhost_2_1 = tk.Label(self.frameMain, image=self.imgGhost_2, bg=self.bgMain);
        self.lblImgGhost_2_1.image = self.imgGhost_2;
        self.lblImgGhost_2_1.place(x=60, y=430);
        #Creación de botones
        self.btnImgReset = tk.Button(self.frameMain, image=self.imgReset, bd=0, highlightthickness=0, bg=self.bgMain, cursor="hand1", activebackground=self.bgMain, command=lambda: self.resetGame(mainWindow));
        self.btnImgReset.image = self.imgReset;
        self.btnImgReset.place(x=330, y=870);
        self.btnImgQuestion = tk.Button(self.frameMain, image=self.imgQuestion, bd=0, highlightthickness=0, bg=self.bgMain, cursor="hand1", activebackground=self.bgMain, command=lambda: self.showRules(mainWindow));
        self.btnImgQuestion.image = self.imgQuestion;
        self.btnImgQuestion.place(x=470, y=870);
        #Cargado de botones dinámicos
        self.loadBtns(mainWindow);
        #Regla para evitar que se redimensione ventana principal
        mainWindow.resizable(height=False, width=False);
    #Método de reinicio del juego
    def resetGame(self, mainWindow):
        result = MessageBox.askquestion("Reinicio", "¿Está seguro de reiniciar la partida actual?");
        if result == "yes":
            #Cargado de botones dinámicos
            self.loadBtns(mainWindow);
    #Método de cargado de botones
    def loadBtns(self, mainWindow):
        self.txtScore.config(text="10");
        self.listData = [];
        nGhosts = 0;
        self.ghostsFinds = 0;
        while nGhosts != 8:
            #Recorrido de filas
            for r in range(0, 10):
                #Creación de lista de filas
                listRow = [];
                #Recorrido de columnas
                for c in range(0, 6):
                    n = random.randint(1, 8);
                    #Creación de botón dinámico
                    if n == 8 and nGhosts != 8:
                        nGhosts = nGhosts + 1;
                        listRow.append("ghost");
                    else:
                        listRow.append("x");
                    #Cargado de imagen por defecto
                    txtPath = "Assets/imgBtnDefault.png";
                    imgBtn = tk.PhotoImage(file=txtPath);
                    btnDinamic = tk.Button(self.framePaint, bd=1, highlightthickness=1, highlightbackground="#000000", image=imgBtn, cursor="hand1", bg=self.bgBtn_1, activebackground=self.bgBtn_1);
                    btnDinamic.image = imgBtn;
                    btnDinamic.config(command=lambda r=r, c=c, btnDinamic=btnDinamic: self.validateField(r, c, btnDinamic, mainWindow));
                    btnDinamic.grid(row=r, column=c, ipadx=8, ipady=8);
                #Guardado de la fila en lista de datos
                self.listData.append(listRow);
            #Validador para saber si reiniciar nuevamente la creación de grilla de botones
            if nGhosts != 8:
                nGhosts = 0;
                self.listaData = [];
    #Método para validar si escogio un fantasma o no
    def validateField(self, row, column, btnDinamic, mainWindow):
        #Valida si el botón ya ha sido seleccionado a partir del background del botón
        if btnDinamic.cget("bg") == self.bgBtn_1:
            #Obtención de valor de puntuación actual
            valueScore = int(self.txtScore.cget("text"));
            #Búsqueda de datos para saber si el botón es una X o un fantasma
            valueBtn = self.listData[row][column];
            txtPath = "";
            if valueBtn == "x":
                txtPath = "Assets/x.png";
                valueScore = valueScore + 2;
            else:
                txtPath = "Assets/ghostPaint.png";
                valueScore = valueScore - 4;
                self.ghostsFinds = self.ghostsFinds + 1;
            #Asignación de puntación al label correspondiente
            self.txtScore.config(text=str(valueScore));
            #Cambio de propiedades al botón seleccionado
            imgBtn = tk.PhotoImage(file=txtPath);
            btnDinamic.config(image=imgBtn, bg=self.bgBtn_2, activebackground=self.bgBtn_2, cursor="arrow");
            btnDinamic.image = imgBtn;
            #Validación para saber si gano o perdio
            if self.ghostsFinds == 3:
                self.showDefeat(mainWindow);
            elif valueScore >= 50:
                self.showVictory(mainWindow);
    #Método que muestra mensaje de derrota al perder
    def showDefeat(self, mainWindow):
        result = MessageBox.askretrycancel("Reintentar", "Lo sentimos a perdido.\n¿Desea intentar nuevamente?");
        if result:
            self.loadBtns(mainWindow);
        else:
            mainWindow.destroy();
    #Método que muestra mensaje de felicitaciones
    def showVictory(self, mainWindow):
        result = MessageBox.askquestion("Ganaste!!!", "Felicidades has ganado.\n¿Deseas jugar nuevamente?");
        if result == "yes":
            #Cargado de botones dinámicos
            self.loadBtns(mainWindow);
        else:
            mainWindow.destroy();
    #Método que muestra las reglas del juego
    def showRules(self, mainWindow):
        MessageBox.showinfo("Reglas", "Las reglas son las siguientes:\n\n- Inicias con 10 puntos.\n- Si al seleccionar una casilla encuentras un fantasma pierdes 4 puntos, si no encuentras un fantasma ganas 2 puntos.\n- Si acumulas 3 fantasmas inmediatamente pierdes.\n- Para ganar tienes que alcanzar una puntuación mínima de 50 puntos.");
